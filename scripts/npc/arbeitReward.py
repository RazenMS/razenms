MAPLE_ADMIN = 2007
sm.setSpeakerID(MAPLE_ADMIN)

if chr.getJob() == 100 and chr.getLevel() >= 30:
    selection = sm.sendSay("Which warrior would you like to be?\r\n#L0##bFighter#l\r\n#L1#Page#l\r\n#L2#Spearman#k#l")
    if selection == 0:
        sm.jobAdvance(110)
        sm.dispose()
    elif selection == 1:
        sm.jobAdvance(120)
        sm.dispose()
    elif selection == 2:
        sm.jobAdvance(130)
        sm.dispose()
if chr.getJob() == 200 and chr.getLevel() >= 30:
    selection = sm.sendSay("Which magician would you like to be?\r\n#L3##bFire Poison Mage#l\r\n#L4#Ice Lightning Mage#l\r\n#L5#Cleric#k#l")
    if selection == 3:
        sm.jobAdvance(210)
        sm.dispose()
    elif selection == 4:
        sm.jobAdvance(220)
        sm.dispose()
    elif selection == 5:
        sm.jobAdvance(230)
        sm.dispose()
if chr.getJob() == 300 and chr.getLevel() >= 30:
    selection = sm.sendSay("Which archer would you like to be?\r\n#L6##bHunter#l\r\n#L7#Crossbowman#l")
    if selection == 6:
        sm.jobAdvance(310)
        sm.dispose()
    elif selection == 7:
        sm.jobAdvance(320)
        sm.dispose()
if chr.getJob() == 400 and chr.getLevel() >= 30:
    selection = sm.sendSay("Which thief would you like to be?\r\n#L8##bAssassian#l\r\n#L9#Bandit#l")
    if selection == 8:
        sm.jobAdvance(410)
        sm.dispose()
    elif selection == 9:
        sm.jobAdvance(420)
        sm.dispose()
if chr.getJob() == 500 and chr.getLevel() >= 30:
    selection = sm.sendSay("Which pirate would you like to be?\r\n#L10##bBrawler#l\r\n#L11#Gunslinger#l")
    if selection == 10:
        sm.jobAdvance(510)
        sm.dispose()
    elif selection == 11:
        sm.jobAdvance(520)
        sm.dispose()
if chr.getJob() == 0 and chr.getLevel() == 10 and chr.getSubJob() == 10:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(508)
        sm.resetAP(False, 508)
        sm.dispose()
if chr.getJob() == 400 and chr.getLevel() == 20 and chr.getSubJob() == 1:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(430)
        sm.dispose()
if chr.getJob() == 430 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(431)
        sm.dispose()
if chr.getJob() == 501 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(530)
        sm.dispose()
if chr.getJob() == 508 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(570)
        sm.dispose()
if chr.getJob() == 431 and chr.getLevel() == 45:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(432)
        sm.dispose()
if chr.getJob() == 110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(111)
        sm.dispose()
if chr.getJob() == 120 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(121)
        sm.dispose()
if chr.getJob() == 130 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(131)
        sm.dispose()
if chr.getJob() == 210 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(211)
        sm.dispose()
if chr.getJob() == 220 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(221)
        sm.dispose()
if chr.getJob() == 230 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(231)
        sm.dispose()
if chr.getJob() == 310 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(311)
        sm.dispose()
if chr.getJob() == 320 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(321)
        sm.dispose()
if chr.getJob() == 410 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(411)
        sm.dispose()
if chr.getJob() == 420 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(421)
        sm.dispose()
if chr.getJob() == 432 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(433)
        sm.dispose()
if chr.getJob() == 510 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(511)
        sm.dispose()
if chr.getJob() == 520 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(521)
        sm.dispose()
if chr.getJob() == 530 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(531)
        sm.dispose()
if chr.getJob() == 570 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(571)
        sm.dispose()
if chr.getJob() == 111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(112)
        sm.dispose()
if chr.getJob() == 121 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(122)
        sm.dispose()
if chr.getJob() == 131 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(132)
        sm.dispose()
if chr.getJob() == 211 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(212)
        sm.dispose()
if chr.getJob() == 221 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(222)
        sm.dispose()
if chr.getJob() == 231 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(232)
        sm.dispose()
if chr.getJob() == 311 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(312)
        sm.dispose()
if chr.getJob() == 321 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(322)
        sm.dispose()
if chr.getJob() == 411 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(412)
        sm.dispose()
if chr.getJob() == 421 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(422)
        sm.dispose()
if chr.getJob() == 433 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(434)
        sm.dispose()
if chr.getJob() == 511 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(512)
        sm.dispose()
if chr.getJob() == 521 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(522)
        sm.dispose()
if chr.getJob() == 531 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(532)
        sm.dispose()
if chr.getJob() == 571 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(572)
        sm.dispose()
if chr.getJob() == 1100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1110)
        sm.dispose()
if chr.getJob() == 1200 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1210)
        sm.dispose()
if chr.getJob() == 1300 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1310)
        sm.dispose()
if chr.getJob() == 1400 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1410)
        sm.dispose()
if chr.getJob() == 1500 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(1510)
        sm.dispose()
if chr.getJob() == 1110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1111)
        sm.dispose()
if chr.getJob() == 1210 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1211)
        sm.dispose()
if chr.getJob() == 1310 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1311)
        sm.dispose()
if chr.getJob() == 1410 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1411)
        sm.dispose()
if chr.getJob() == 1510 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(1511)
        sm.dispose()
if chr.getJob() == 1111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1112)
        sm.dispose()
if chr.getJob() == 1211 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1212)
        sm.dispose()
if chr.getJob() == 1311 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1312)
        sm.dispose()
if chr.getJob() == 1411 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(1412)
        sm.dispose()
if chr.getJob() == 1511 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you lke to advance?"):
        sm.jobAdvance(1512)
        sm.dispose()
if chr.getJob() == 2100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2110)
        sm.dispose()
if chr.getJob() == 2110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2111)
        sm.dispose()
if chr.getJob() == 2111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2112)
        sm.dispose()
if chr.getJob() == 2001 and chr.getLevel() == 10:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2210)
        sm.dispose()
if chr.getJob() == 2003 and chr.getLevel() == 10:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2400)
        sm.resetAP(False, 2400)
        sm.dispose()
if chr.getJob() == 2210 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2212)
        sm.dispose()
if chr.getJob() == 2212 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2214)
        sm.dispose()
if chr.getJob() == 2214 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2218)
        sm.dispose()
if chr.getJob() == 2300 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2310)
        sm.dispose()
if chr.getJob() == 2310 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2311)
        sm.dispose()
if chr.getJob() == 2311 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2312)
        sm.dispose()
if chr.getJob() == 2400 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2410)
        sm.dispose()
if chr.getJob() == 2410 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2411)
        sm.dispose()
if chr.getJob() == 2411 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2412)
        sm.dispose()
if chr.getJob() == 2500 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2510)
        sm.dispose()
if chr.getJob() == 2510 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2511)
        sm.dispose()
if chr.getJob() == 2511 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2512)
        sm.dispose()
if chr.getJob() == 2700 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2710)
        sm.dispose()
if chr.getJob() == 2710 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2711)
        sm.dispose()
if chr.getJob() == 2711 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(2712)
        sm.dispose()
if chr.getJob() == 3100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3110)
        sm.dispose()
if chr.getJob() == 3101 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3120)
        sm.dispose()
if chr.getJob() == 3200 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3210)
        sm.dispose()
if chr.getJob() == 3300 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3310)
        sm.dispose()
if chr.getJob() == 3500 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3510)
        sm.dispose()
if chr.getJob() == 3600 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3610)
        sm.dispose()
if chr.getJob() == 3700 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3710)
        sm.dispose()
if chr.getJob() == 3110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3111)
        sm.dispose()
if chr.getJob() == 3120 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3121)
        sm.dispose()
if chr.getJob() == 3210 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3211)
        sm.dispose()
if chr.getJob() == 3310 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3311)
        sm.dispose()
if chr.getJob() == 3510 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3511)
        sm.dispose()
if chr.getJob() == 3610 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3611)
        sm.dispose()
if chr.getJob() == 3710 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3711)
        sm.dispose()
if chr.getJob() == 3111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3112)
        sm.dispose()
if chr.getJob() == 3121 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3122)
        sm.dispose()
if chr.getJob() == 3211 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3212)
        sm.dispose()
if chr.getJob() == 3311 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3312)
        sm.dispose()
if chr.getJob() == 3511 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3512)
        sm.dispose()
if chr.getJob() == 3611 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3612)
        sm.dispose()
if chr.getJob() == 3711 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(3712)
        sm.dispose()
if chr.getJob() == 4100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4110)
        sm.dispose()
if chr.getJob() == 4200 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4210)
        sm.dispose()
if chr.getJob() == 4110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4111)
        sm.dispose()
if chr.getJob() == 4210 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4211)
        sm.dispose()
if chr.getJob() == 4111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4112)
        sm.dispose()
if chr.getJob() == 4211 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(4212)
        sm.dispose()
if chr.getJob() == 5100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(5110)
        sm.dispose()
if chr.getJob() == 5110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(5111)
        sm.dispose()
if chr.getJob() == 5111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(5112)
        sm.dispose()
if chr.getJob() == 6100 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6110)
        sm.dispose()
if chr.getJob() == 6110 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6111)
        sm.dispose()
if chr.getJob() == 6111 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6112)
        sm.dispose()
if chr.getJob() == 6500 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6510)
        sm.dispose()
if chr.getJob() == 6510 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6511)
        sm.dispose()
if chr.getJob() == 6511 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(6512)
        sm.dispose()
if chr.getJob() == 14000 and chr.getLevel() == 10:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(14200)
        sm.resetAP(False, 14200)
        sm.dispose()
if chr.getJob() == 14200 and chr.getLevel() >= 30:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(14210)
        sm.dispose()
if chr.getJob() == 14210 and chr.getLevel() >= 60:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(14211)
        sm.dispose()
if chr.getJob() == 14211 and chr.getLevel() >= 100:
    if sm.sendAskYesNo("Would you like to advance?"):
        sm.jobAdvance(14212)
        sm.dispose()
else:
    sm.sendSayOkay("You can't job advance at this time.")
    sm.dispose()
