package net.swordie.ms.constants;

public class CustomConstants {
    //Buffed mobs
    public static final int BUFFED_MOB_HP_MULTIPLIER = 20;
    public static final int BUFFED_MOB_SCALE = 100; //Default scale is 100
    public static final int BUFFED_MOB_DAMAGE_MULTIPLIER = 10;
    public static final int BUFFED_MOB_EXP_MULTIPLIER = 5;

    public static final boolean AUTO_AGGRO = true;
}
